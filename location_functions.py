# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 09:58:43 2018

@author: jand
"""

import numpy as np

def linrot(m,Nsta,Ndat,Ndatev,Npar,Nevent,xr,yr,zr,maxpert,beta):

    NDM = 50
    ntot = Ndat
    dat1      = np.zeros(ntot)
    dat2      = np.zeros(ntot)
    dRdm      = np.zeros((Npar,ntot,NDM))
    dm_start  = np.zeros(Npar)
    Jac       = np.zeros((ntot,Npar))
    JactCdinv = np.zeros((ntot,ntot))
    Cdinv     = np.zeros((ntot,ntot))
    Mpriorinv = np.zeros((Npar,Npar))
    Ctmp      = np.zeros((Npar,Npar))
    VT        = np.zeros((Npar,Npar))
    V         = np.zeros((Npar,Npar))
    L         = np.zeros(Npar)
    pcsd      = np.zeros(Npar)
    
    dm_start = m*0.1
    sigma = m[-2:]

    for ip in range(Npar):
        dm = np.zeros(NDM)
        dm[0] = dm_start[ip] # Estimate deriv for range of dm values
        for i in range(NDM):
            #print i,ip
            mtry1 = np.copy(m)
            mtry1[ip] = mtry1[ip]+dm[i]
            #dat1 = get_times(m=mtry1,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
            for iev in range(Nevent):
                mtmp = np.append(mtry1[iev*4:(iev+1)*4],mtry1[-4:])
                dtmp = get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
                ist = iev*Ndatev
                iend = (iev+1)*Ndatev
                dat1[ist:iend] = dtmp

            mtry2 = np.copy(m)
            mtry2[ip] = mtry2[ip]-dm[i]
            #dat2 = get_times(m=mtry2,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
            for iev in range(Nevent):
                mtmp = np.append(mtry2[iev*4:(iev+1)*4],mtry2[-4:])
                dtmp = get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
                ist = iev*Ndatev
                iend = (iev+1)*Ndatev
                dat2[ist:iend] = dtmp

            for j in range(ntot):
                if(np.abs((dat1[j]-dat2[j])/(dat1[j]+dat2[j])) > 1.0e-7):
                    dRdm[ip,j,i] = (dat1[j]-dat2[j])/(2.*dm[i])
                else:
                    dRdm[ip,j,i] = 0.
            if(i < NDM-1):
                dm[i+1] = dm[i]/1.5

        for j in range(ntot):   # For each datum, choose best derivative estimate
            best = 1.e10
            ibest = 1
            for i in range(NDM-2):
                if((np.abs(dRdm[ip,j,i+0]) < 1.0e-7) or 
                (np.abs(dRdm[ip,j,i+1]) < 1.0e-7) or 
                (np.abs(dRdm[ip,j,i+2]) < 1.0e-7)):
                    test = 1.e20
                else:
                    test = np.abs((dRdm[ip,j,i+0]/dRdm[ip,j,i+1] +
                           dRdm[ip,j,i+1]/dRdm[ip,j,i+2])/2.-1.0)
                                
                if((test < best) and (test > 1.e-7)):
                    best  = test
                    ibest = i+1
            Jac[j,ip] = dRdm[ip,j,ibest]  # Best deriv into Jacobian
            if(best > 1.e10):
                Jac[j,ip] = 0.

    for i in range(Npar):   # Scale columns of Jacobian for stability
        Jac[:,i] = Jac[:,i]*maxpert[i]

    for i in range(Npar):
        Mpriorinv[i,i] = 12.          # variance for U[0,1]=1/12

    i=0
    for iev in range(Nevent):
        for ifq in range(2):
            for ista in range(Nsta):
                Cdinv[i,i] = beta/sigma[ifq]**2
                i+=1
    JactCdinv = np.matmul(np.transpose(Jac),Cdinv)
    Ctmp = np.matmul(JactCdinv,Jac) + Mpriorinv
    #Ctmp = np.linalg.inv(np.matmul(JactCdinv,Jac) + Mpriorinv)
    
    V,L,VT = np.linalg.svd(Ctmp)
    pcsd = 0.5*(1./np.sqrt(np.abs(L))) # PC standard deviations
    #pcsd = np.sqrt(L) # PC standard deviations
    
#    for i in range(Npar):
#        if (pcsd[i] > 1./np.sqrt(12.)):
#            pcsd[i] = 1./np.sqrt(12.)

#    print('***************************')
#    print('***************************')
#    print(pcsd)
#    print('***************************')
#    print('***************************')
    return(V,pcsd)

def get_times(m,xr,yr,zr,Nsta):
    #print('m:',m)
    dtry = np.zeros(2*Nsta)
    dtry[0:Nsta]=travel_time3D(x=m[0],y=m[1],z=m[2],xr=xr,yr=yr,zr=zr,V=m[4],tori=m[3])
    dtry[Nsta:2*Nsta]=travel_time3D(x=m[0],y=m[1],z=m[2],xr=xr,yr=yr,zr=zr,V=m[5],tori=m[3])
    return(dtry)
def travel_time3D(x,y,z,xr,yr,zr,V,tori):
    #print(x,y,z,V,tori)
    t=tori+np.sqrt((xr-x)**2+(yr-y)**2+(zr-z)**2)/V
    return(t)
