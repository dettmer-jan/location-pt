# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 11:00:43 2018
Plotting for MCMC results after running EQL_MCMC.py
@author:
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import location_functions as lf
import matplotlib as mpl

def mymap():
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    # create colormap
    # ---------------
    # create a colormap that consists of
    # - 1/5 : custom colormap, ranging from white to the first color of the colormap
    # - 4/5 : existing colormap
    # set upper part: 4 * 256/4 entries
    upper = mpl.cm.jet(np.arange(256))
        # set lower part: 1 * 256/4 entries
    # - initialize all entries to 1 to make sure that the alpha channel (4th column) is 1
    lower = np.ones((int(256/4),4))
    # - modify the first three columns (RGB):
    #   range linearly between white (1,1,1) and the first color of the upper colormap
    for i in range(3):
      lower[:,i] = np.linspace(1, upper[0,i], lower.shape[0])
    # combine parts of colormap
    cmap = np.vstack(( lower, upper ))
    # convert to matplotlib colormap
    cmap = mpl.colors.ListedColormap(cmap, name='myColorMap', N=cmap.shape[0])

#    fig, ax = plt.subplots()
#    x,y = np.meshgrid(np.linspace(0,99,100),np.linspace(0,99,100))
#    z   = (x-50)**2. + (y-50)**2.
#    im = ax.imshow(z, interpolation='nearest', cmap=cmap, clim=(0,5000))
#    div  = make_axes_locatable(ax)
#    cax  = div.append_axes('bottom', size='5%', pad=0.4)
#    cbar = plt.colorbar(im, cax=cax, orientation='horizontal')
#    plt.show()
    return cmap



plt.close('all')

idata = False
nbin    = 60
Nevent  = 30
ext     = ''
Neventmax = Nevent
Nsta    = 60
Npar    = Nevent * 4 + 4
NDAT = 2*Nsta*Nevent    #number of data (both P- and S- phases)
Ndatev = 2*Nsta
borealis=False #turn on naming convention for borealis 
Nchain=1 #number of chain: The number of random starting points

#dataset folder
dataset_folder='/home/jand/location_pt/'
out_dir=dataset_folder+'out/example_linrot'+str(Nevent)+ext+'/'
#out_dir=dataset_folder+'out/example/2M_iter/'

##load the files
df=pd.read_csv(out_dir+"st_example.csv")
xr=df['xr'].values
yr=df['yr'].values
zr = np.zeros(xr.shape)

df=pd.read_csv(out_dir+"Mprior_example.csv") 
mt=df['mt'].values
mt_name=df['mt_name'].values
mt_unit=df['mt_unit'].values
minlim=df['minlim'].values
maxlim=df['maxlim'].values
maxpert = maxlim - minlim;
minlim = minlim-maxpert/50.
maxlim = maxlim-maxpert/50.

mt_nameeq = ['x','y','z','tori']               #naming convention for our parameters
mt_uniteq = [' (km)',' (km)',' (km)',' (s)']     #unit naming convention for our parameters
mt_nameenv = ['vp','vs','$\sigma_P$', '$\sigma_S$']               #naming convention for our parameters
mt_unitenv = [' (km/s)',' (km/s)','','']     #unit naming convention for our parameters

mt_name = mt_nameeq
mt_unit = mt_uniteq
for ev in range(Nevent-1):
    mt_name = np.append(mt_name,mt_nameeq)
    mt_unit = np.append(mt_unit,mt_uniteq)    
mt_name = np.append(mt_name,mt_nameenv)
mt_unit = np.append(mt_unit,mt_unitenv)


df=pd.read_csv(out_dir+"logLkeep2_example.csv")   
logLkeep2=df[np.append('logLkeep2','beta')].values

df=pd.read_csv(out_dir+"mkeep2_example.csv")   
mkeep2=df.values[:,1:Npar+1]
print('No. samples:',len(mkeep2))

#Cov=np.zeros((len(mt),len(mt),Nchain)) 
#for ichain in np.arange(Nchain):
#    df=pd.read_csv(out_dir+"Cov_example_"+str(ichain)+".csv")
#    for idx in np.arange(0,len(mt)):
#        Cov[:,idx,ichain]=df['%s'%(str(idx))].values

#using borealis stations, change station 6,7,8... to 10,11,12 ...
if borealis:
    sta_bor=['01','02','03','04','05','10','11','12','13','14']      
    sta_name=[] #station name
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_P'%(sta_bor[ista])) #station name for using P-arrivals
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_S'%(sta_bor[ista])) #station name for using P-arrivals
else:
    sta_name=[] #station name
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_P'%(str(ista+1))) #station name for using P-arrivals
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_S'%(str(ista+1))) #station name for using P-arrivals

if idata:        
    df=pd.read_csv(out_dir+"dkeep2_example.csv")   
    dkeep2=df[np.append(sta_name,'ichain')].values

df=pd.read_csv(out_dir+"acc_example.csv")   
acc=df.values[:,1:]

NMCMC= float(len(acc))    #number of MCMC steps
print('No. samples:',len(acc))
Nburnin = int(NMCMC/3.)

if idata:
    dtru = np.zeros((NDAT),dtype=float); #initialize true forward model (a vector)
    #dtru = lf.get_times(m=mt,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
    for iev in range(Nevent):
        mtmp = np.append(mt[iev*4:(iev+1)*4],mt[-4:])
        dtmp = lf.get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
        ist = iev*Ndatev
        iend = (iev+1)*Ndatev
        dtru[ist:iend] = dtmp

plt.figure()
plt.plot(logLkeep2[:,0])

############################################
################### Plotting MCMC results
#plotting chain histories 
fig = plt.figure(figsize=(12,10))
fig.subplots_adjust(hspace=.075, wspace=.5)
isub = 0
#isub += 1
#plt.subplot(Npar+1,Nchain,isub)
#plt.plot(logLkeep2[:,0],'g')
#plt.ylabel('Misfit E')
#plt.xlim([-NMCMC/75.,NMCMC])
for ipar in np.arange(Npar-4,Npar):
    isub += 1
    ax=fig.add_subplot(Neventmax+1,4,isub)
    if isub <= Nevent*4:
        ax.set_xticklabels([])
    else:
        ax.set_xlabel('MCMC Index')
    plt.plot(mkeep2[:,ipar],'-k',alpha=0.6)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'-w',lw=2)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'--k',lw=2)
    plt.ylabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([-NMCMC/75.,NMCMC])
    plt.ylim([minlim[ipar],maxlim[ipar]])
print(Npar)
for ipar in np.arange(Npar-4):
    isub += 1
    print(isub)
    ax=fig.add_subplot(Neventmax+1,4,isub)
    if isub <= Nevent*4:
        ax.set_xticklabels([])
    else:
        ax.set_xlabel('MCMC Index')
    plt.plot(mkeep2[:,ipar],'-k',alpha=0.6)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'-w',lw=2)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'--k',lw=2)
    plt.ylabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([-NMCMC/75.,NMCMC])
    plt.ylim([minlim[ipar],maxlim[ipar]])
#fig.tight_layout()
plt.rc({'font.size': 10})
plt.show()
plt.savefig(out_dir+'par_chains_all.png', dpi = 150,bbox_inches='tight')

#================================================
# Delete Burnin
logLkeep2= logLkeep2[Nburnin:]
mkeep2 = mkeep2[Nburnin:,:]
if idata:
    dkeep2 = dkeep2[Nburnin:,:]
acc = acc[Nburnin:]

NMCMC= float(len(acc))

plt.figure()
plt.plot(logLkeep2[:,0])

#plotting chain histories after removing burn-in
fig = plt.figure(figsize=(12,10))
fig.subplots_adjust(hspace=.075, wspace=.5)
isub = 0
#isub += 1
#plt.subplot(Npar+1,Nchain,isub)
#plt.plot(logLkeep2[:,0],'g')
#plt.ylabel('Misfit E')
#plt.xlim([-NMCMC/75.,NMCMC])
for ipar in np.arange(Npar-4,Npar):
    isub += 1
    ax=fig.add_subplot(Neventmax+1,4,isub)
    if isub <= Nevent*4:
        ax.set_xticklabels([])
    else:
        ax.set_xlabel('MCMC Index')
    plt.plot(mkeep2[:,ipar],'-k',alpha=0.6)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'-w',lw=2)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'--k',lw=2)
    plt.ylabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([-NMCMC/75.,NMCMC])
    plt.ylim([minlim[ipar],maxlim[ipar]])
for ipar in np.arange(Npar-4):
    isub += 1
    ax=fig.add_subplot(Neventmax+1,4,isub)
    if isub <= Nevent*4:
        ax.set_xticklabels([])
    else:
        ax.set_xlabel('MCMC Index')
    plt.plot(mkeep2[:,ipar],'-k',alpha=0.6)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'-w',lw=2)
    plt.plot([1,NMCMC],[mt[ipar],mt[ipar]],'--k',lw=2)
    plt.ylabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([-NMCMC/75.,NMCMC])
    plt.ylim([minlim[ipar],maxlim[ipar]])
#fig.tight_layout()
plt.rc({'font.size': 10})
plt.show()
plt.savefig(out_dir+'par_chains.png', dpi = 150,bbox_inches='tight')

#plotting 1D marginals:
fig = plt.figure(figsize=(16,9))
fig.subplots_adjust(hspace=.6, wspace=.075)
isub = 0
#isub += 1
#ax=plt.subplot(Nchain,Npar+1,isub)
#ax.set_yticklabels([])
#plt.hist(logLkeep2[:,0],nbin,color='g', normed=1, alpha=0.75, edgecolor = "none")
#plt.ylabel('Misfit E')
#plt.title('ichain: '+str(ichain))
for ipar in np.arange(Npar-4,Npar):
    isub += 1
    ax = fig.add_subplot(Neventmax/2+1,8,isub)
    plt.hist(mkeep2[:,ipar],nbin, normed=True, edgecolor='none',facecolor='gray')
    plt.axvline(mt[ipar], color='k', linestyle='dashed', linewidth=2)
    plt.xlabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([minlim[ipar],maxlim[ipar]])
    ax.set_yticklabels([])
#isub += 4
print(Npar,isub)
for ipar in np.arange(Npar-4):
    isub += 1
    ax = fig.add_subplot(Neventmax/2+1,8,isub)
    plt.hist(mkeep2[:,ipar],nbin, normed=True, edgecolor='none',facecolor='gray')
    plt.axvline(mt[ipar], color='k', linestyle='dashed', linewidth=2)
    plt.xlabel(mt_name[ipar]+'%s'%mt_unit[ipar])
    plt.xlim([minlim[ipar],maxlim[ipar]])
    ax.set_yticklabels([])
#    if (isub-1) % 4 == 0:
#        ax.set_ylabel('Probability')
#fig.tight_layout()
plt.rc({'font.size': 10})
plt.show()
plt.savefig(out_dir+'par_hist.png', dpi = 150,bbox_inches='tight')

##############Ploting 2D histogram
fig=plt.figure(figsize=(12,12))
mycmap = mymap()
for iev in range(Nevent):
#    plt.hist2d(mkeep2[:,iev*4],mkeep2[:,(iev*4)+1], bins=nbin, cmap=plt.cm.jet)
    plt.hist2d(mkeep2[:,iev*4],mkeep2[:,(iev*4)+1], bins=nbin, cmap=mycmap)
for st_id in np.arange(len(xr)): #loop over stations
    plt.plot(xr[st_id],yr[st_id],"v",markerfacecolor='black', \
    markeredgewidth=2, markeredgecolor='white',markersize=20)
plt.xlabel(mt_name[0]+' (km)')
plt.ylabel(mt_name[1]+' (km)')
plt.xlim(minlim[0], maxlim[0])
plt.ylim(minlim[1], maxlim[1])
#plt.colorbar()
plt.axes().set_aspect('equal')
plt.rc({'font.size': 12})
plt.margins(tight=True)
plt.show()
plt.savefig(out_dir+'location_map.png', dpi = 150,bbox_inches='tight')
plt.tight_layout()

############ Plotting Acceptance Curve
fig = plt.figure(figsize=(16,9))
fig.subplots_adjust(hspace=.6, wspace=.075)
isub = 0
for ipar in np.arange(Npar):
    isub += 1
    ax = fig.add_subplot(Neventmax/2+1,8,isub)
    plt.hist(acc[:,ipar],nbin, normed=True, edgecolor='none',facecolor='gray')
    ax.set_yticklabels([])
plt.legend(loc='best')
plt.title('Acceptance Rate')
plt.show()

############## Plotting Residual in Data
##plotting Vp Data
#plt.figure(figsize=(8,10));
#for ipar in np.arange(nstat):
#    plt.subplot(nstat,2,2*ipar+1);
#    plt.hist(dkeep2[:,ipar],20, normed=1, alpha=0.75, edgecolor = "none");
#    plt.axvline(dtru[ipar], color='k', linestyle='dashed', linewidth=2)
#    plt.ylabel('st: '+str(ipar+1))
#    plt.xlim(minlim[5],maxlim[5]) #axis is the origin time limit
#    if ipar==0:
#        plt.title('Data from Vp (hist)')
#        
#    plt.subplot(nstat,2,2*ipar+2);
#    plt.hist(dkeep2[:,ipar]-dtru[ipar],40,color='red', normed=1, alpha=0.75,label='std '+str(round(np.std(dkeep2[:,ipar]-dtru[ipar]),2)), edgecolor = "none");
#    plt.axvline(0, color='k', linestyle='dashed', linewidth=2)
##    plt.xlim([-max(abs(dkeep2[:,ipar]-dtru[ipar])),max(abs(dkeep2[:,ipar]-dtru[ipar]))])
#    plt.xlim([-0.75,0.75])
#
#    if ipar==0:
#        plt.title('Residual Plot of the Data')
#    plt.legend(loc='lower right', handlelength=0)
#plt.show()
#
##plotting Vs Data
#plt.figure(figsize=(8,10));
#for ipar in np.arange(nstat):
#    plt.subplot(nstat,2,2*ipar+1);
#    plt.hist(dkeep2[:,ipar+nstat],20, normed=1, alpha=0.75, edgecolor = "none");
#    plt.axvline(dtru[ipar+nstat], color='k', linestyle='dashed', linewidth=2)
#    plt.ylabel('st: '+str(ipar+1))
#    plt.xlim(minlim[5],maxlim[5]) #axis is the origin time limit
#    if ipar==0:
#        plt.title('Data from Vs (hist)')
#        
#    plt.subplot(nstat,2,2*ipar+2);
#    plt.hist(dkeep2[:,ipar+nstat]-dtru[ipar+nstat],40,color='red', normed=1, alpha=0.75,label='std '+str(round(np.std(dkeep2[:,ipar+nstat]-dtru[ipar+nstat]),2)), edgecolor = "none");
#    plt.axvline(0, color='k', linestyle='dashed', linewidth=2)
##    plt.xlim([-max(abs(dkeep2[:,ipar+nstat]-dtru[ipar+nstat])),max(abs(dkeep2[:,ipar+nstat]-dtru[ipar+nstat]))])
#    plt.xlim([-0.75,0.75])
#
#    if ipar==0:
#        plt.title('Residual Plot of the Data')  
#    plt.legend(loc='lower right', handlelength=0)
#plt.show()
