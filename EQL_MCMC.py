# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 14:13:06 2018
MCMC_vs_GridSearch
###########################################################
##################### MCMC ################################   
###########################################################
@author: root
"""
import numpy as np
from mpi4py import MPI
import pandas as pd
import os, sys
import time
import matplotlib.pyplot as plt
import location_functions as lf

np.set_printoptions(precision=3)
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
status = MPI.Status()

tstart=time.time() #starting time to keep
plt.close('all')
#np.random.seed(10)
#print('random seed is activated')

##############################################################################
########################### GLOBAL PARAMETERS ################################
Nevent = 30
ext = ''
Npha = 2
borealis= False #if using borealis stations convention
irot = True #toggle for rotation
ilinrot = True
itemper = True
Nchain = comm.Get_size()-1 # No. MCMC chains or MPI threads

iwrite = True #toggle to write output files

cconv = 0.3
hconv = 0.05
nbin = 200
dTlog = 1.5
stepscale = 1.1
Nchain_conv = 2 # Run 2 independent chains to test convergence

## MCMC sampling, initializing parameters
Nburnin = 10000 #index for burning
NKEEP = 1000 #index for writing it down
NMCMC = 100000*NKEEP; #number of random walks
NCHAINTHIN = 10
N_AFTER_ROT = 40000 # Do at least N_AFTER_ROT steps after nonlinear rotation started

##############################################################################
##############################################################################

dataset_folder='/home/jand/location_pt/'
out_dir=dataset_folder+'out/example_linrot'+str(Nevent)+ext+'/'
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

ichain_id=[] #ichain_id is for naming convention of the chain
for jchain in np.arange(Nchain):
    ichain_id.append('ichain_'+str(jchain))
 
ttmp=time.time()
#print('time1:',ttmp-tstart)
mt_nameeq = ['x','y','z','tori']               #naming convention for our parameters
mt_uniteq = ['km','km','km','s']     #unit naming convention for our parameters
mt_nameenv = ['vp','vs','\sigma_P','\sigma_S']               #naming convention for our parameters
mt_unitenv = ['km/s','km/s','','']     #unit naming convention for our parameters

mt_name = mt_nameeq
mt_unit = mt_uniteq
for ev in range(Nevent-1):
    mt_name = np.append(mt_name,mt_nameeq)
    mt_unit = np.append(mt_unit,mt_uniteq)    
mt_name = np.append(mt_name,mt_nameenv)
mt_unit = np.append(mt_unit,mt_unitenv)

if borealis:
    Nsta=10
    zr=np.zeros(Nsta)
        ############borealis stations:
    xr = np.array([43.033220766923044, 51.738289975117084, 36.96756876339084,
         59.7109885923992, 71.6763933032984, 22.24596664589884, 45.04771539958456, 
         67.7008499729696, 67.7008499729696, 61.89487513806703],dtype=float)
     
    yr = np.array([51.27527688531686, 29.658791009932862, 41.98128130358993,
         42.70671448986165, 31.626630289013224, 57.59759951510833, 41.348602675369385,
         44.62647801043663, 36.007036864811646, 22.24596664589884],dtype=float)

    minlimeq = np.array([ 0., 0., 0.,  0.],dtype=float)  #minimum prior
    maxlimeq = np.array([94.,80.,35., 10.],dtype=float)  #maximum prior
    minlimenv=np.array([3., 2., 0., 0.],dtype=float)  #minimum prior
    maxlimenv=np.array([7., 6., 1., 1.],dtype=float)  #maximum prior

    sta_bor=['01','02','03','04','05','10','11','12','13','14']      
    Npar=len(mt_name) #length of parameter
    sta_name=[] #station name
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_P'%(sta_bor[ista])) #station name for using P-arrivals
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_S'%(sta_bor[ista])) #station name for using S-arrivals
else:
    Nsta=100
    zr=np.zeros(Nsta)
    # Priors:
    minlimeq = np.array([ 0., 0., 0.,  0.],dtype=float)  #minimum prior
    maxlimeq = np.array([94.,80.,35., 10.],dtype=float)  #maximum prior
    minlimenv=np.array([3., 2., 0., 0.],dtype=float)  #minimum prior
    maxlimenv=np.array([7., 6., 1., 1.],dtype=float)  #maximum prior

    #STATION LOCATIONS:
    #xr=np.array([1, 1,19,19, 1, 19,10,10],dtype=float) #location of the receivers
    #yr=np.array([1,19, 1,19,10,10,19,1],dtype=float)  #location of the receivers
    #xr=np.random.rand(Nsta)*(maxlimeq[0]-minlimeq[0])  #location of the receivers
    #yr=np.random.rand(Nsta)*(maxlimeq[1]-minlimeq[1])  #location of the receivers
    xr = np.array([66.21704103, 93.88334948, 22.9991129 , 13.33761978, 23.80300316,
       90.2466171 , 57.48473982, 43.42853476, 65.92748832, 14.67916724,
       76.88580004, 52.31033983, 89.81669539, 23.69326061, 41.22096277,
       93.81087213, 26.61796559, 76.97765488, 53.3770315 , 11.29153808,
        1.97472989,  5.56906436, 92.360779  ,  0.99651428, 39.85367778,
       81.27462197,  6.16000604, 35.77601293, 74.73192684, 72.41260934,
       12.25409829, 89.61083175, 42.46105705, 65.89061409, 16.39555341,
       44.54820596, 31.64356558, 69.40905453, 73.964957  , 46.72485807,
        6.76025152, 56.32573787, 59.29342446, 32.22464394, 44.55849439,
       21.1304924 , 83.5090966 , 51.26639248, 36.71819353, 74.98971756,
       63.05366835, 40.94308083, 30.55502238, 47.908141  , 73.17701019,
       41.35149984, 30.13388397, 46.05510912, 18.80370634, 17.53261466,
       36.39718245, 84.55123665, 81.75193171,  6.98808856, 32.73577106,
       13.07907437, 89.36503033, 44.97178891, 11.02201473, 47.62572312,
       40.9137193 , 92.30394087, 56.70489983, 19.61168551, 60.95282646,
       35.08699957, 90.26669131, 52.72787704, 10.78561662, 16.62689426,
       18.83926562, 71.27369586, 85.24522079, 27.93042562, 60.98604687,
       13.27448871, 68.32210745, 48.22769621, 90.94075219, 24.09559263,
       24.88125143,  7.81119017, 65.95848566, 17.00878397, 77.58705228,
        3.94659716, 50.87730616, 45.6787956 , 40.83613908, 24.22001773])
    yr = np.array([74.01031343,  7.52240721, 27.93424137, 63.39483683, 40.78577655,
       61.01234621, 37.64167023, 43.12106023, 23.56115439, 34.09758631,
       46.34999986, 78.20128862, 60.3827756 , 73.04335614, 47.14897113,
        6.32896141,  6.38218158, 27.76216625, 24.75501069, 51.34979003,
       11.66336045, 69.62802973, 22.31784468, 33.86064176, 21.62751293,
       79.21642362, 20.94097425, 57.30785968, 22.64090756,  8.38756036,
       65.91182079,  9.45408192, 29.95343686, 55.72706272, 32.06705348,
       55.05894238, 43.26907054, 42.00846766, 25.88239588, 22.50505162,
       25.71624103, 56.16629803, 73.09825507, 66.06216878, 76.41842984,
       47.32735154, 57.22090712, 52.5507427 , 66.48751507, 32.0741417 ,
       18.27508904, 63.7985898 , 69.57665137, 40.90350859, 26.36261786,
       48.56114714, 78.85275098,  7.60987911, 47.61293257, 18.63726414,
       35.26849311, 72.59567973, 59.05755393, 48.90963805, 25.61142629,
       26.78822201, 25.28427409, 61.23230095, 50.91636598, 65.44693876,
       50.45256832, 15.32684195, 71.4729723 , 33.64193435, 38.35326742,
       39.88285086, 16.10741236,  6.93973221, 37.70190846, 79.09958115,
       42.00251502, 47.36525481, 48.77807835,  8.01611287, 52.38225358,
       21.32146424, 62.27576716, 18.91681194,  2.97223638, 33.07466629,
       64.58653769, 22.41059892, 29.29897074, 10.33271754, 41.91585537,
       25.44969488, 67.54555948, 19.08050305, 15.63823092, 44.46899414])
    Npar=len(mt_name) #length of parameter
    sta_name=[] #station name
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_P'%(str(ista+1))) #station name for using P-arrivals
    for ista in np.arange(len(xr)):
        sta_name.append('st%s_S'%(str(ista+1))) #station name for using S-arrivals

sta_name_all = []
for ev in range(Nevent):
    for ista in range(2*len(xr)):
        sta_name_all.append(sta_name[ista])

minlim = minlimeq
maxlim = maxlimeq
for ev in range(Nevent-1):
    minlim = np.append(minlim,minlimeq)
    maxlim = np.append(maxlim,maxlimeq)    
minlim = np.append(minlim,minlimenv)
maxlim = np.append(maxlim,maxlimenv)

##
## Parametrization chosen to be: (lat, lon, dep, tau) for each event and 
## (Vp, Vs, sig_p, sig_s) apply to all events
##
Npar = Nevent*4 + 4  #number of parameter

maxpert = maxlim - minlim;
mt = np.zeros(Npar)
#mt[:Nevent*4]=minlim[:Nevent*4] + maxpert[:Nevent*4] * np.random.rand(np.size(mt[:Nevent*4])) #randomly generating initial parameter
if Nevent > 100:
    ## mtru for 4 events inside array:
    mt[:Nevent*4]=minlim[:Nevent*4]+maxpert[:Nevent*4]*np.random.rand(Nevent*4)
if Nevent == 30:
    ## mtru for 4 events inside array:
    mt[:Nevent*4]=np.array([10.,10.,15.,1.5,
                            10.,20.,20.,1.5,
                            20.,20.,20.,1.5,
                            20.,10.,20.,1.5,
                            25.,20.,20.,1.5,
                            25.,10.,20.,1.5,
                            30.,10.,10.,0.8,
                            30.,15.,10.,0.8,
                            30.,20.,10.,0.8,
                            30.,25.,20.,0.8,
                            30.,30.,20.,0.8,
                            30.,35.,20.,0.8,
                            35.,40.,10.,0.8,
                            35.,50.,10.,0.8,
                            40.,40.,10.,0.8,
                            41.,41.,20.,0.8,
                            40.,41.,20.,0.8,
                            41.,40.,20.,0.8,
                            42.,42.,20.,0.8,
                            42.,41.,20.,0.8,
                            41.,42.,20.,0.8,
                            50.,45., 8.,1.5,
                            55.,50.,20.,0.3,
                            55.,40.,20.,0.2,
                            60.,35.,10.,1.,
                            60.,30.,12.,0.5,
                            65.,40.,15.,2.,
                            65.,50.,10.,1.,
                            70.,30.,12.,0.5,
                            70.,40.,15.,2.,
                            ],dtype=float)
if Nevent == 6:
    ## mtru for 4 events inside array:
    mt[:Nevent*4]=np.array([60.,35.,10.,1.,70.,40.,15.,2.,50.,45.,8.,1.5,10.,20.,20.,1.5,65.,30.,12.,0.5,35.,50.,30.,0.8],dtype=float)
if Nevent == 5:
    ## mtru for 4 events inside array:
    mt[:Nevent*4]=np.array([60.,35.,10.,1.,70.,40.,15.,2.,50.,45.,8.,1.5,10.,20.,20.,1.5,65.,30.,12.,0.5],dtype=float)
if Nevent == 4:
    ## mtru for 4 events inside array:
    mt[:Nevent*4]=np.array([60.,35.,10.,1.,70.,40.,15.,2.,50.,45.,8.,1.5,10.,20.,20.,1.5],dtype=float)
if Nevent == 3:
    ## mtru for 3 events inside array:
    mt[:Nevent*4]=np.array([60.,35.,10.,1.,70.,40.,15.,2.,50.,45.,8.,1.5],dtype=float)
if Nevent == 2:
    ## mtru for 2 events inside array:
    mt[:Nevent*4]=np.array([60.,35.,10.,1.,70.,40.,15.,2.],dtype=float)
if Nevent == 1:
    if ext == 'a':
        mt[:Nevent*4]=np.array([60.,35.,10.,1.],dtype=float)
    if ext == 'b':
        mt[:Nevent*4]=np.array([70.,40.,15.,2.],dtype=float)
    if ext == 'c':
        mt[:Nevent*4]=np.array([50.,45.,8.,1.5],dtype=float)
    if ext == 'd':
        mt[:Nevent*4]=np.array([10.,20.,20.,1.5],dtype=float)
    if ext == 'e':
        mt[:Nevent*4]=np.array([65.,30.,12.,0.5],dtype=float)
    if ext == 'f':
        mt[:Nevent*4]=np.array([35.,50.,30.,0.8],dtype=float)  
mt[-4:] = np.array([6., 3.47, 0.05, 0.3],dtype=float)  #true model of the earthquake hypocentre: x,y,z,vp,vs,tori




#saving prior model and station lat long to csv
df=pd.DataFrame({'mt':mt,'mt_name':mt_name,'mt_unit':mt_unit,'minlim':minlim,'maxlim':maxlim})
df.to_csv(out_dir+"Mprior_example.csv") #save prior model to csv        

#saving location of stations to csv
df=pd.DataFrame({'xr':xr,'yr':yr})
df.to_csv(out_dir+"st_example.csv") #save prior model to csv     

##
## Parallel tempering schedule:
##
if(Nchain > 1):
    Nptchains  = Nchain    # Number of chains (equal to number of workers)
    if itemper:
        #Nptchains1 = 1 
        Nptchains1 = int(np.ceil(Nptchains/3))
    else:
        Nptchains1 = Nptchains       
    NT = Nptchains-Nptchains1+1
    Nchaint = np.zeros(NT,dtype=int)
    beta_pt = np.zeros(Nptchains,dtype=float)
    ibeta_pt = np.zeros(Nptchains,dtype=int)
    Nchaint[:] = 1
    Nchaint[0] = Nptchains1

    if(rank == 0):
        print('Nchain',Nchain,'NT',NT)
        print('Nchaint',Nchaint)
    it2 = 0
    for it in range(NT):
        for ic in range(Nchaint[it]):
            beta_pt[it2] = 1./dTlog**float(it)
            ibeta_pt[it2] = it+1
            if(rank == 0):
                print('Chain ',it2,':  beta = ',beta_pt[it2],'  T = ',1./beta_pt[it2],'iT',ibeta_pt[it2])
            it2 = it2 + 1
if rank > 0:
    beta_chain = beta_pt[rank-1]
    ibeta_chain = ibeta_pt[rank-1]
    print('rank & beta',rank,beta_chain,ibeta_chain)
else:
    beta_chain = 1.
comm.Barrier()

ttmp=time.time()
#print('time2:',ttmp-tstart)

Ndat = 2*Nsta*Nevent # Total No. data
Ndatev = 2*Nsta # No. data per event
Ndatevp = Nsta # No. data per event
Ndatevs = Nsta # No. data per event

mkeep2 = np.zeros((NKEEP,Npar+1),dtype=float) #initialize parameter keep (a matrix), +1 for assigning ichain
mpair = np.zeros((Npar,2),dtype=float) #initialize parameter keep (a matrix), +1 for assigning ichain
logLkeep2 = np.zeros((NKEEP,2),dtype=float)  # Buffer for logL and beta_chain
swapkeep = np.zeros(NKEEP,dtype=float)  # Buffer for logL and beta_chain
logLpair = np.zeros(2,dtype=float)
logLpairold = np.zeros(2,dtype=float)
betapair = np.zeros(2,dtype=float)
ibetapair = np.zeros(2,dtype=int)
acckeep2 = np.zeros((NKEEP,Npar),dtype=float) # acceptance ratio
prop = np.zeros((Nchain,Npar),dtype=int)  # Counters on master
acc = np.zeros((Nchain,Npar),dtype=int)  # Counters on master
ipropacc = np.zeros((2,Npar),dtype=int)  # Counters on workers

logLcur = 0.
logLtmp = np.zeros(Nevent)
dkeep2 = np.zeros((NKEEP,Ndat+1),dtype=float) #initialize forward model keep (a matrix), +1 for assigning ichain
dtry = np.zeros((Ndat),dtype=float) #initialize 
dtru = np.zeros((Ndat),dtype=float); #initialize true forward model (a vector)
dobs = np.zeros((Ndat),dtype=float); #initialize d obs
u = np.zeros((Npar,Npar),dtype=float) #initialize u for SVD
s = np.zeros((Npar),dtype=float) #initialize u for SVD
vh = np.zeros((Npar,Npar),dtype=float) #initialize u for SVD
pcsd = np.zeros(Npar,dtype=float) #initialize pcsd for SVD
mcsum = np.zeros((Npar,Npar),dtype=float) #initialize covariance matrix sum
Cov = np.zeros((Npar,Npar),dtype=float) #initialize covariance matrix
mmsum = np.zeros((Npar),dtype=float) #initiazlize parameter mean vector: becareful with the dimension of Nx1 vs just N (a vector)
hist_m = np.zeros((nbin+1,Npar),dtype=float) #initialize histogram of model parameter, 10 bins -> 11 edges by Npar
mnew = np.zeros(Npar,dtype=float)
mcur = np.zeros(Npar,dtype=float)
mtmp0 = np.zeros(Npar,dtype=float)
mtmp1 = np.zeros(Npar,dtype=float)
msend = np.zeros(Npar+3,dtype=float)
mbar = np.zeros(Npar,dtype=float)
dnew = np.zeros(Ndat,dtype=float); #initialize dnew using both Vp and Vs
dcur = np.zeros(Ndat,dtype=float); #initialize dcur using both Vp and Vs
R = np.zeros((Npar,Npar),dtype=float) #initialize correlation matrix
R[:,:] = R[:,:] + 1.

pcsd[:] = 1./100.
u[:,:] = np.eye(Npar)

ttmp=time.time()
#print('time3:',ttmp-tstart)

noise = np.zeros(Nevent*Nsta*Npha,dtype=float)
if rank == 0:
    #noise = np.random.randn(Nevent*Nsta*Npha)
    #np.savetxt('noise.txt',noise)
    tmp = np.loadtxt('noise.txt')
    noise[:]=tmp[:Nevent*Nsta*Npha]
comm.Bcast(noise, root=0)

##calculate dtru
for iev in range(Nevent):
    mtmp = np.append(mt[iev*4:(iev+1)*4],mt[-4:])
    #print(mtmp)
    dtmp = lf.get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
    ist = iev*Ndatev
    iend = (iev+1)*Ndatev
    dtru[ist:iend] = dtmp
    #dobs[ist:ist+Nsta]=dtru[ist:ist+Nsta]+mt[-2]*np.random.randn(Nsta)
    #dobs[ist+Nsta:iend]=dtru[ist+Nsta:iend]+mt[-1]*np.random.randn(Nsta)
    dobs[ist:ist+Nsta]=dtru[ist:ist+Nsta]+mt[-2]*noise[ist:ist+Nsta]
    dobs[ist+Nsta:iend]=dtru[ist+Nsta:iend]+mt[-1]*noise[ist+Nsta:iend]
    logLtmp[iev]= -Ndatevp/2.*np.log(mt[-2])-np.sum((dobs[ist:ist+Nsta] -dtmp[0:Nsta])**2)/(2.*mt[-2]**2) \
                  -Ndatevs/2.*np.log(mt[-1])-np.sum((dobs[ist+Nsta:iend]-dtmp[Nsta:])**2)/(2.*mt[-1]**2)    
logLtru = np.sum(logLtmp)
#if rank == 1:
print('True likelihood = ',logLtru,'rank',rank)

ttmp=time.time()

###Counter / Toggle Variables
ihead = True
ikeep = 0 #counter when writing to output files
ncov = 0
c_new = 0
icount = 0
hist_d_plot=[] # Record hist_dif in a list

irot_start = False
irot_msg = True
t1 = time.time()
i_after_rot = 0

##
## WORKER
##
if rank > 0:
    #######
    ## Define starting model for the 2 MCMC chains:
    #ibreak = 0 
    #while ibreak == 0:
    #    mnew = mt + pcsd*np.random.randn(len(mt))
    #    # Check that mtry is inside unifrom prior:
    #    if ((maxlim-mnew).min()>0.) & ((mnew-minlim).min()>0.):
    #        ibreak = 1
    mnew = minlim + maxpert*np.random.rand(len(mt))
    #mcur = np.copy(mnew)
    mcur = np.copy(mt)
    #dnew = lf.get_times(m=mnew,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
    for iev in range(Nevent):
        mtmp = np.append(mcur[iev*4:(iev+1)*4],mcur[-4:])
        dtmp = lf.get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
        ist = iev*Ndatev
        iend = (iev+1)*Ndatev
        logLtmp[iev]= -Ndatevp/2.*np.log(mcur[-2])-np.sum((dobs[ist:ist+Nsta] -dtmp[0:Nsta])**2)/(2.*mcur[-2]**2) \
                      -Ndatevs/2.*np.log(mcur[-1])-np.sum((dobs[ist+Nsta:iend]-dtmp[Nsta:])**2)/(2.*mcur[-1]**2)    
    logLcur = np.sum(logLtmp)
    if rank == 1:
        print('Starting likelihood = ',logLcur)
    #dcur = np.copy(dnew)
    ##
    ## LINROT
    ##
    if ilinrot:
        if rank == 1:
            tlin1 = time.time()
        u,pcsd = lf.linrot(m=mcur,Nsta=Nsta,Ndat=Ndat,
                           Ndatev=Ndatev,Npar=Npar,Nevent=Nevent,xr=xr,
                           yr=yr,zr=zr,maxpert=maxpert,beta=beta_chain)
        ut = np.transpose(u)
        if rank == 1:
            tlin2 = time.time()
            print('Time for computing Jacobian: ',tlin2-tlin1,'s')
    for imcmc in np.arange(NMCMC): #loop to number of MCMC random walk
        #t1=time.time()
        #c_diff = np.max(np.max(np.abs(R[:,:,0]-R[:,:,1])))
        #if((c_diff < cconv) & irot_msg & irot): #when activating irot_start
        if((imcmc > Nburnin) & irot_msg & irot): #when activating irot_start
            irot_start = True
            irot_msg = False
            ipropacc[:,:] = 1
            print('Starting PC sampling with nonlinear estimate at imcmc=',imcmc)
        if irot_msg == False:
            i_after_rot += 1
        Nrand = int(np.random.rand(1)*NCHAINTHIN)+1
        for ichainthin in np.arange(Nrand):
            for ipar in np.arange(Npar): #loop to number of parameter
                if ilinrot:
                    mtry=(mcur- minlim)/maxpert
                    mtry=np.matmul(ut,mtry) #with rotation
                    ## Cauchy proposal:
                    mtry[ipar] = mtry[ipar]+ stepscale*pcsd[ipar]*np.tan(np.pi*(np.random.rand(1)-0.5))
                    mtry= np.matmul(u,mtry)
                    mtry=minlim+(mtry*maxpert)
                else:
                    mtry=(mcur- minlim)/maxpert
                    ## Cauchy proposal:
                    mtry[ipar] = mtry[ipar]+ stepscale*pcsd[ipar]*np.tan(np.pi*(np.random.rand(1)-0.5))
                    mtry=minlim+(mtry*maxpert)
                    
                ipropacc[0,ipar] += 1   
                if ((maxlim-mtry).min()>0.) & ((mtry-minlim).min()>0.):  # Check that mtry is inside unifrom prior:
        
                    #calculate logL for each event
                    logLtmp[:] = 0.
                    for iev in range(Nevent):
                        mtmp = np.append(mtry[iev*4:(iev+1)*4],mtry[-4:])
                        dtmp = lf.get_times(m=mtmp,xr=xr,yr=yr,zr=zr,Nsta=Nsta)
                        ist = iev*Ndatev
                        iend = (iev+1)*Ndatev
                        #dtry[ist:iend] = dtmp
                        logLtmp[iev]= -Ndatevp/2.*np.log(mtry[-2])-np.sum((dobs[ist:ist+Nsta] -dtmp[0:Nsta])**2)/(2.*mtry[-2]**2) \
                                      -Ndatevs/2.*np.log(mtry[-1])-np.sum((dobs[ist+Nsta:iend]-dtmp[Nsta:])**2)/(2.*mtry[-1]**2)
                    logLtry = np.sum(logLtmp)
                    #logLtry = -Ndat/2.*np.log(mtry[-1])-np.sum((dobs-dtry)**2)/(2*mtry[-1]**2);
        
                    # Compute likelihood ratio in log space:
                    dlogL = (logLtry-logLcur)*beta_chain
                    # Apply MH criterion (accept/reject)
                    if (np.random.rand(1) < np.exp(dlogL)):
                        # Accept:
                        ipropacc[1,ipar] += 1
                        logLcur = np.copy(logLtry)
                        mcur = np.copy(mtry)
                        #dcur = np.copy(dtry)
    
        if (icount > NKEEP)  & (c_new >= 1): #c_new 2 is for activating rotation
            c_new = 2
            icount = 0
        if (icount > Nburnin) & (c_new == 0):
            icount = 0
            c_new = 1
            ncov = 0.
            mcsum = 0.
            mmsum = 0.
             
        mw = (mcur - minlim)/maxpert    # for covariance        
        ncov += 1.
        mmsum = mmsum + mw #calculating the sum of mean(m)
        mbar = mmsum/ncov #calculating covariance matrix
        mcsum = mcsum + np.outer(np.transpose(mw-mbar),mw-mbar)
        if (c_new == 2) & (icount == 0):
            Cov = mcsum/ncov #calculating covariance matrix
            for ipar in range(Npar):
                for jpar in range(Npar):
                    R[ipar,jpar]=Cov[ipar,jpar]/np.sqrt(Cov[ipar,ipar]*Cov[jpar,jpar])
            if irot_start & irot: #ROTATION 
                u,s,vh=np.linalg.svd(Cov) #rotate it to its Singular Value Decomposition
                ut = np.transpose(u)
                pcsd=np.sqrt(s)
        icount += 1    #counter for rotation matrix
        ## Sending model to Master
        msend[:] = 0.
        msend[0] = logLcur
        msend[1] = beta_chain
        msend[2] = float(ibeta_chain)
        msend[3:] = mcur[:]
        comm.Send(msend, dest=0, tag=rank)
        comm.Send(ipropacc, dest=0, tag=rank)
        ## Receiving back from Master
        msend[:] = 0.
        comm.Recv(msend, source=0, tag=MPI.ANY_TAG)
        mcur[:] = msend[3:]
        logLcur = msend[0]
        beta_chain = msend[1]
        ibeta_chain = msend[2]
        #print('rank',rank)
        #print(logLcur,mcur)
        #if rank == 1:
        #    print('rank',rank)
        #    print(ipropacc[1,:]/ipropacc[0,:])
        #    print(pcsd)
        #    print('')
##
## MASTER
##
else:
    isave = 0
    swapacc = 1
    swapprop = 1
    logLpairold[:] = 0.
    tkeep0=time.time()
    tkeep00=time.time()

    for imcmc in np.arange(NMCMC):
        ##
        ## Receive from workers
        ##
        msend[:] = 0.
        comm.Recv(msend, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG,status=status)
        isource0 = status.source
        comm.Recv(ipropacc, source=isource0, tag=MPI.ANY_TAG)
        mpair[:,0] = msend[3:]
        logLpair[0] = msend[0]
        betapair[0] = msend[1]
        ibetapair[0] = int(msend[2])
        prop[isource0-1,:] = ipropacc[0,:]
        acc[isource0-1,:] = ipropacc[1,:]

        msend[:] = 0.
        comm.Recv(msend, source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG,status=status)
        isource1 = status.source
        comm.Recv(ipropacc, source=isource1, tag=MPI.ANY_TAG)
        mpair[:,1] = msend[3:]
        logLpair[1] = msend[0]
        betapair[1] = msend[1]
        ibetapair[1] = int(msend[2])
        prop[isource1-1,:] = ipropacc[0,:]
        acc[isource1-1,:] = ipropacc[1,:]
        ##
        ## Tempering exchange move
        ##
        iswap = 0
        if itemper & (ibetapair[0] != ibetapair[1]):
            betaratio = betapair[1]-betapair[0]
            logratio  = betaratio*(logLpair[0]-logLpair[1])
            if(np.random.rand(1) < np.exp(logratio)):
                ## ACCEPT SWAP
                mtmp0[:] = np.copy(mpair[:,0])
                mtmp1[:] = np.copy(mpair[:,1])
                mpair[:,0] = np.copy(mtmp1)
                mpair[:,1] = np.copy(mtmp0)
                logLtmp0 = np.copy(logLpair[0])
                logLtmp1 = np.copy(logLpair[1])
                logLpair[0] = np.copy(logLtmp1)
                logLpair[1] = np.copy(logLtmp0)
                #print('P1:',logLpair[0],mpair[:,0])
                #print('P2:',logLpair[1],mpair[:,1])
                iswap = 1
                if (np.abs(ibetapair[0]-ibetapair[1])==1):
                    swapacc = swapacc+1
            if (np.abs(ibetapair[0]-ibetapair[1])==1):
                swapprop = swapprop+1
        msend[:] = 0.
        msend[0] = np.copy(logLpair[0])
        msend[1] = np.copy(betapair[0])
        msend[2] = np.copy(float(ibetapair[0]))
        msend[3:] = np.copy(mpair[:,0])
        comm.Send(msend, dest=isource0, tag=rank)
        msend[:] = 0.
        msend[0] = np.copy(logLpair[1])
        msend[1] = np.copy(betapair[1])
        msend[2] = np.copy(float(ibetapair[1]))
        msend[3:] = np.copy(mpair[:,1])
        comm.Send(msend, dest=isource1, tag=rank)
        #print('Acceptance, isource',acc[isource0-1,:]/prop[isource0-1,:],isource0,betapair[0])
        #print('Acceptance, isource',acc[isource1-1,:]/prop[isource1-1,:],isource1,betapair[1])

#        if  (c_new[ichain] >= 1): #calculate the difference between ichain parameters
#            #calculating histograms
#            for ipar in np.arange(len(mt)):
#                edge=np.linspace(minlim[ipar],maxlim[ipar],nbin+1)
#                idx_dif=np.argmin(abs(edge-mcur[ipar,ichain]))
#                hist_m[idx_dif,ipar,ichain] += 1        
        ##
        ## Saving sample into buffers
        ##
        if iwrite:
            if ibetapair[0] == 1:
                logLkeep2[ikeep,:-1] = logLpair[0]
                logLkeep2[ikeep,-1] = betapair[0]
                mkeep2[ikeep,:-1] = mpair[:,0]
                mkeep2[ikeep,-1] = isource0
                #dkeep2[ikeep,:] = np.append(dcur[:,ichain],isource0)
                acckeep2[ikeep,:] = (acc[isource0-1,:]).astype(float)/(prop[isource0-1,:]).astype(float)
                swapkeep[ikeep] = iswap
                #print(ibetapair[0],betapair[0])
                isave += 1
                ikeep += 1
            if (ibetapair[1] == 1) & (ikeep<NKEEP):
                logLkeep2[ikeep,:-1] = logLpair[1]
                logLkeep2[ikeep,-1] = betapair[1]
                mkeep2[ikeep,:-1] = mpair[:,1]
                mkeep2[ikeep,-1] = isource0
                #dkeep2[ikeep,:] = np.append(dcur[:,ichain],isource0)
                acckeep2[ikeep,:] = (acc[isource1-1]).astype(float)/(prop[isource1-1]).astype(float)
                #print(ibetapair[1],betapair[1])
                isave += 1
                ikeep += 1
#        #SUBTRACTING 2 NORMALIZED HISTOGRAM
#        if (imcmc > Nburnin): # Save after burn-in
#            hist_dif=((np.abs(hist_m[:,:,0]/hist_m[:,:,0].max()-hist_m[:,:,1]/hist_m[:,:,1].max())).max()) #find the max of abs of the difference between 2 models
#            if (hist_dif < hconv) & (i_after_rot > N_AFTER_ROT):
#                print('Nchain models have converged, terminating.')
#                print('imcmc: '+str(imcmc))
#    
#                df=pd.DataFrame(logLkeep2,columns=['logLkeep2','ichain'])
#                df.to_csv(out_dir+"logLkeep2_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False) #save logLkeep2 to csv          
#                df = pd.DataFrame(mkeep2,columns=np.append(mt_name,'ichain'))
#                df.to_csv(out_dir+"mkeep2_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False) #save mkeep2 to csv
#                df=pd.DataFrame(dkeep2,columns=np.append(sta_name_all,'ichain'))
#                df.to_csv(out_dir+"dkeep2_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False) #save dkeep2 to csv
#                df=pd.DataFrame(acc,columns=np.append('acc','ichain'))
#                df.to_csv(out_dir+"acc_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False) #save acc to csv                          
#                df=pd.DataFrame(hist_d_plot)
#                df.to_csv(out_dir+"Conv_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False)
#                
#                for ichain_id in np.arange(Nchain):
#                    df=pd.DataFrame(Cov[:,:,ichain_id])
#                    df.to_csv(out_dir+"Cov_example_"+str(ichain_id)+".csv") #save covariance matrix to csv 
#                    
#                print("saving output files %.1d %% "%(float(imcmc)/NMCMC*100))
#                print('Rotation is %s'%('on' if (irot) else 'off') )
#                tend=time.time() #starting time to keep
#                print('time to converge: %s sec'%(round(tend-tstart,2)))
#    
#                #TERMINATE!
#                sys.exit('Converged, terminate.')
#    
#            elif (np.mod(imcmc,5*NKEEP)) == 0:
#                t2 = time.time()
#                print('Runtime: ',t2-t1,'s;  imcmc: '+str(imcmc),'hist_diff: %1.3f, cov_diff: %1.3f'%(hist_dif,c_diff))
#                hist_d_plot.append(hist_dif)                
#
        if (iwrite): #dump to a file
            if (ikeep == NKEEP): #dump to a file
                df=pd.DataFrame(logLkeep2[:,:],columns=['logLkeep2','beta'])
                df.to_csv(out_dir+"logLkeep2_example.csv",mode= 'w' if (ihead) else 'a',header= True if (ihead) else False) #save logLkeep2 to csv          
                df = pd.DataFrame(mkeep2[:,:],columns=np.append(mt_name,'ichain'))
                df.to_csv(out_dir+"mkeep2_example.csv",mode= 'w' if (ihead) else 'a',header= True if (ihead) else False) #save mkeep2 to csv
                #df=pd.DataFrame(dkeep2,columns=np.append(sta_name_all,'ichain'))
                #df.to_csv(out_dir+"dkeep2_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False) #save dkeep2 to csv
                df=pd.DataFrame(acckeep2[:,:],columns=[mt_name])
                df.to_csv(out_dir+"acc_example.csv",mode= 'w' if (ihead) else 'a',header= True if (ihead) else False) #save acc to csv        
                df=pd.DataFrame(swapkeep[:],columns=['iswap'])
                df.to_csv(out_dir+"iswap_example.csv",mode= 'w' if (ihead) else 'a',header= True if (ihead) else False) #save acc to csv        
                #df=pd.DataFrame(hist_d_plot)
                #df.to_csv(out_dir+"Conv_example.csv",mode= 'w' if (ihead==1) else 'a',header= True if (ihead==1) else False)
        
                #for ichain_id in np.arange(Nchain):
                #    df=pd.DataFrame(Cov[:,:,ichain_id])
                #    df.to_csv(out_dir+"Cov_example_"+str(ichain_id)+".csv") #save covariance matrix to csv
                tkeep1=time.time()
                print('Sample size:',isave)
                print('imcmc:',imcmc,'Time for Nkeep = ',tkeep1-tkeep0,'s','Total time = ',(tkeep1-tkeep00)/3600.,'h')
                print('logL(m_tru):',logLtru,'logL(mcur) pair:',logLpair,'beta pair:',betapair)
                print('PT swap acceptance:',float(swapacc)/float(swapprop))
                print('In-chain acceptance, isource',acckeep2[ikeep-1,:],mkeep2[ikeep-1,-1])
                #print('In-chain acceptance, isource',acckeep2[ikeep-1,:],mkeep2[ikeep-1,-1])
                #print('Acceptance, isource',acc[isource0-1,:]/prop[isource0-1,:],isource0,betapair[0])
                #print('Acceptance, isource',acc[isource1-1,:]/prop[isource1-1,:],isource1,betapair[1])

                logLkeep2[:,:] = 0.
                mkeep2[:,:] = 0.
                swapkeep[:] = 0
                acckeep2[:,:] = 0.
                ihead = False
                ikeep = 0
                tkeep0=time.time()

tend=time.time() #starting time to keep
print('Total time: %s sec'%(round(tend-tstart,2)))
print('Not converged.')
