#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 09:18:03 2019
Reads output of EQL_MCMC.py into memory for inference purposes.
@author: jand
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import location_functions as lf
import matplotlib as mpl

idata = False
nbin    = 60
Nevent  = 30
ext     = ''
Neventmax = Nevent
Nsta    = 30
Npar    = Nevent * 4 + 4
NDAT = 2*Nsta*Nevent    #number of data (both P- and S- phases)
Ndatev = 2*Nsta

#dataset folder
dataset_folder='/home/jand/location_pt/'
out_dir=dataset_folder+'out/example_linrot'+str(Nevent)+ext+'/'
#out_dir=dataset_folder+'out/example/2M_iter/'

##load the files
df=pd.read_csv(out_dir+"st_example.csv")
xr=df['xr'].values
yr=df['yr'].values
zr = np.zeros(xr.shape)

df=pd.read_csv(out_dir+"Mprior_example.csv") 
mt=df['mt'].values
mt_name=df['mt_name'].values
mt_unit=df['mt_unit'].values
minlim=df['minlim'].values
maxlim=df['maxlim'].values
maxpert = maxlim - minlim;
minlim = minlim-maxpert/50.
maxlim = maxlim-maxpert/50.

mt_nameeq = ['x','y','z','tori']               #naming convention for our parameters
mt_uniteq = [' (km)',' (km)',' (km)',' (s)']     #unit naming convention for our parameters
mt_nameenv = ['vp','vs','$\sigma_P$', '$\sigma_S$']               #naming convention for our parameters
mt_unitenv = [' (km/s)',' (km/s)','','']     #unit naming convention for our parameters

mt_name = mt_nameeq
mt_unit = mt_uniteq
for ev in range(Nevent-1):
    mt_name = np.append(mt_name,mt_nameeq)
    mt_unit = np.append(mt_unit,mt_uniteq)    
mt_name = np.append(mt_name,mt_nameenv)
mt_unit = np.append(mt_unit,mt_unitenv)


df=pd.read_csv(out_dir+"logLkeep2_example.csv")   
logLkeep2=df[np.append('logLkeep2','beta')].values

df=pd.read_csv(out_dir+"mkeep2_example.csv")   
mkeep2=df.values[:,1:Npar+1]
isrc=df.values[:,Npar+1]
print('No. samples:',len(mkeep2))

sta_name=[] #station name
for ista in np.arange(len(xr)):
    sta_name.append('st%s_P'%(str(ista+1))) #station name for using P-arrivals
for ista in np.arange(len(xr)):
    sta_name.append('st%s_S'%(str(ista+1))) #station name for using P-arrivals

if idata:        
    df=pd.read_csv(out_dir+"dkeep2_example.csv")   
    dkeep2=df[np.append(sta_name,'ichain')].values

df=pd.read_csv(out_dir+"acc_example.csv")   
acc=df.values[:,1:]

df=pd.read_csv(out_dir+"iswap_example.csv")   
iswap=df.values[:,1]

NMCMC= float(len(acc))    #number of MCMC steps
print('No. samples:',len(acc))
Nburnin = int(NMCMC/3.)
